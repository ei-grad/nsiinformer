/* 
 * @Author: manson
 * @Date:   2014-12-25 12:33:33
 * @Last Modified by:   manson
 * @Last Modified time: 2014-12-25 16:07:24
 */

var signals = require('./Signals.js').Signals;
var siteDetector = require('./siteDetector.js');


var books = [];
var internalTimer;
var mainLoopTimer;
var internalInterval = 1000 * 30; // 30 secs to check if we need to proceed checkings
var mainLoopInterval = 1000; // internal main loop interval to see if we may procees next query for specified site
var queryIntervals = {}; // for storing query intervals for different sites 
var isUpdating = false; // global updates checking loop running
var isChecking = false; // local updates checking loop running


signals.receive("checkCommentsStart", function(dta) {
    console.log(timeStamp() + '|>commentsObserver "checkCommentsStart" event received');
    mainLoop();
});

signals.receive("checkCommentsStop", function(dta) {
    console.log(timeStamp() + '|>commentsObserver "checkCommentsStop" event received');
    stop();
});

function timeStamp() {
    return '[' + new Date() + '] ';
}


function start() {
    console.log(timeStamp() + '|>commentsObserver started.');
    correctCommentsUpdateTime(function() {
        mainLoop();
        startTimer();
    });
}

function stop() {
    console.log(timeStamp() + '|>commentsObserver stoped.');
    stopTimer();
}


function startTimer() {
    //isUpdating = false;internalInterval
    internalTimer = setInterval(mainLoop, internalInterval);
}

function stopTimer() {
    if (internalTimer)
        clearInterval(internalTimer);
    books = []; // if internal loop is active - break it om next step (next author)
}
// if checkCommentsAt is undefind we will not be able to find record with this empty field, so we set it with current datetime (if checkUpdates is true)
function correctCommentsUpdateTime(cb) {
    Service.loadBooks({
        checkComments: true,
    }, function(err, books) {

        var update = function(_books, callback) {
            if (_books.length === 0) return callback();
            var book = _books.shift();
            if (!book.checkCommentsAt && book.siteTemplate) {
                book.checkCommentsAt = new Date();
                var cd = siteDetector.getSiteByTemplate(book.siteTemplate);
                if (cd)
                    cd.storage.comments.update(book, function(err) {
                        // if error - it already was logged in book storage
                        update(_books, callback);
                    }); // async calling all
                else {
                    console.log(timeStamp() + '[commentsObserver] Warning: not found site for siteTemplate: ' + book.siteTemplate + ' and book: ' + book.title);
                    update(_books, callback);
                }
            } else
                update(_books, cb);
        }

        update(books, cb);
    });
}

function mainLoop() {
    if (isUpdating) return;
    isUpdating = true;
    Service.loadBooks({
        checkComments: true,
        checkCommentsAt: {
            '<=': new Date()
        }
    }, function(err, _books) {
        if (_books.length === 0) {
            //console.log('nothing to check');
            isUpdating = false;
            return;
        }
        console.log(timeStamp() + '|>commentsObserver found ' + _books.length + ' to check. Checking loop has started...');
        books = _books;
        // start checkings books who needs to be checked
        mainLoopTimer = setInterval(mainLoopPrivate, mainLoopInterval);
    });

}


var postponedChecking = false;
// check author if specified period between queries finished
function mainLoopPrivate() {
    if (isChecking) return;
    if (!books || books.length === 0) {
        clearInterval(mainLoopTimer);
        isUpdating = false;
        isChecking = false;
        return;
    }
    isChecking = true;

    var book = books.shift(); // get the first book from array
    var site = siteDetector.getSiteByTemplate(book.siteTemplate);
    if (!site) {
        console.log(timeStamp() + '|>commentsObserver ERROR. Not found site for  ' + book.title);
        isChecking = false; // unlock main loop to check other books
        return;
    }
    //queryIntervals[site.siteTemplate] = 
    var httpQueryInterval = site.schedulers.comments.httpQueryInterval;
    if (
        (queryIntervals[site.siteTemplate] && queryIntervals[site.siteTemplate] < new Date()) ||
        !queryIntervals[site.siteTemplate]
    ) {
        queryIntervals[site.siteTemplate] = (new Date()).addSeconds(httpQueryInterval); // set next approved time to check for this site
        //console.log('set next round trip: ' + queryIntervals[site.siteTemplate]);
    } else { // we cannot make queries to book site 
        if (!postponedChecking) {
            postponedChecking = true;
            console.log(timeStamp() + '|>commentsObserver waiting for site querying. Next book comments will be checked in ' + queryIntervals[site.siteTemplate]);
        }
        books.push(book); // put book to the end of books array
        isChecking = false; // unlock main loop to check other books
        return;
    }
    postponedChecking = false;
    // check the comments
    signals.signal("check_comments_start", book); // broadcast info
    Service.checkComments(book.commentsHref, function(err, result) {
        signals.signal("check_comments_end", {
            book: book,
            result: result
        });
        if (err) {
            console.log(timeStamp() + '|>commentsObserver. ERROR "' + book.title + '" NOT checked. ' + err);
            isChecking = false; // unlock main loop to check other books
            return;
        } else {
            if (result.success) {
                console.log(timeStamp() + '|>commentsObserver. Book "' + book.title + '" checked and it ' + ((result.commentsHasUpdates) ? 'has NEW comments!' : 'has NO new comments'));
            }
            if (result.success && result.book) {
                // set next update time
                book = site.schedulers.comments.calculateNextCheckTime(result.book);
                console.log(timeStamp() + '|>commentsObserver. Comments for book "' + book.title + '" next time will be checked in ' + book.checkCommentsAt);
                // save book
                site.storage.comments.update(book, function() {
                    isChecking = false; // unlock main loop to check other books
                    Book.publishUpdate(book.id,book);
                    return;
                });
            } else {
                isChecking = false; // unlock main loop to check other books
                return;
            }
        }
    });

}



exports.start = start;
exports.stop = stop;

start();