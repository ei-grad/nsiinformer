/* 
 * @Author: manson
 * @Date:   2014-12-25 12:33:33
 * @Last Modified by:   manson
 * @Last Modified time: 2014-12-25 16:07:24
 */

var signals = require('./Signals.js').Signals;
var siteDetector = require('./siteDetector.js');


var writers = [];
var internalTimer;
var mainLoopTimer;
var internalInterval = 1000 * 30; // 30 secs to check if we need to proceed checkings
var mainLoopInterval = 1000; // internal main loop interval to see if we may procees next query for specified site
var queryIntervals = {}; // for storing query intervals for different sites 
var isUpdating = false; // global updates checking loop running
var isChecking = false; // local updates checking loop running


signals.receive("checkBooksUpdatesStart", function(dta) {
    console.log(timeStamp() + '|>booksUpdatesObserver "checkBooksUpdatesStart" event received');
    mainLoop();
});

signals.receive("checkBooksUpdatesStop", function(dta) {
    console.log(timeStamp() + '|>booksUpdatesObserver "checkBooksUpdatesStop" event received');
    stop();
});

//signals.signal("myEvent", "data to send in event...");


function timeStamp() {
    return '[' + new Date() + '] ';
}


function start() {
    console.log(timeStamp() + '|>booksUpdatesObserver started.');
    correctAuthorsUpdateTime(function() {
        mainLoop();
        startTimer();
    });
}

function stop() {
    console.log(timeStamp() + '|>booksUpdatesObserver stoped.');
    stopTimer();
}


function startTimer() {
    //isUpdating = false;internalInterval
    internalTimer = setInterval(mainLoop, internalInterval);
}

function stopTimer() {
    if (internalTimer)
        clearInterval(internalTimer);
    writers = []; // if internal loop is active - break it on next step (next author)
}
// if checkUpdatesAt is undefind we will not be able to find record with this empty field, so we set it with current datetime (if checkUpdates is true)
function correctAuthorsUpdateTime(cb) {
    Service.loadAuthors({
        checkUpdates: true,
    }, function(err, authors) {
        if (err){
            console.log(timeStamp() + '[booksUpdatesObserver] Error: ' + err);
            return;
        }
        var update = function(authors, callback) {
            if (authors.length === 0) return callback();
            var author = authors.shift();
            if (!author.checkUpdatesAt && author.siteTemplate) {
                author.checkUpdatesAt = new Date();
                var cd = siteDetector.getSiteByTemplate(author.siteTemplate);
                if (cd)
                    cd.storage.author.update(author, function(err) {
                        // if error - it already was logged in author storage
                        update(authors, callback);
                    }); // async calling all
                else {
                    console.log(timeStamp() + '[booksUpdatesObserver] Warning: not found site for siteTemplate: ' + author.siteTemplate + ' and author: ' + author.name);
                    update(authors, callback);
                }
            } else
                update(authors, cb);
        };

        update(authors, cb);
    });
}

function mainLoop() {
    if (isUpdating) return;
    isUpdating = true;
    Service.loadAuthors({
        checkUpdates: true,
        checkUpdatesAt: {
            '<=': new Date()
        }
    }, function(err, authors) {
        if (authors.length === 0) {
            //console.log('nothing to check');
            isUpdating = false;
            return;
        }
        console.log(timeStamp() + '|>booksUpdatesObserver found ' + authors.length + ' to check. Checking loop has started...');
        writers = authors;
        // start checkings writers who needs to be checked
        mainLoopTimer = setInterval(mainLoopPrivate, mainLoopInterval);
    });

}


var postponedChecking = false;
// check author if specified period between queries finished
function mainLoopPrivate() {
    if (isChecking) return;
    if (!writers || writers.length === 0) {
        clearInterval(mainLoopTimer);
        isUpdating = false;
        isChecking = false;
        return;
    }
    isChecking = true;

    var writer = writers.shift(); // get the first writer from array
    var site = siteDetector.getSiteByTemplate(writer.siteTemplate);
    if (!site) {
        console.log(timeStamp() + '|>booksUpdatesObserver ERROR. Not found site for  ' + writer.name);
        isChecking = false; // unlock main loop to check other writers
        return;
    }
    //queryIntervals[site.siteTemplate] = 
    var httpQueryInterval = site.schedulers.author.httpQueryInterval;
    if (
        (queryIntervals[site.siteTemplate] && queryIntervals[site.siteTemplate] < new Date()) ||
        !queryIntervals[site.siteTemplate]
    ) {
        queryIntervals[site.siteTemplate] = (new Date()).addSeconds(httpQueryInterval); // set next approved time to check for this site
        //console.log('set next round trip: ' + queryIntervals[site.siteTemplate]);
    } else { // we cannot make queries to writer site 
        if (!postponedChecking) {
            postponedChecking = true;
            console.log(timeStamp() + '|>booksUpdatesObserver waiting for site querying. Next author will be checked in ' + queryIntervals[site.siteTemplate]);
        }
        writers.push(writer); // put writer to the end of writers array
        isChecking = false; // unlock main loop to check other writers
        return;
    }
    postponedChecking = false;
    // check the author
    signals.signal("check_author_start", writer); // broadcast info
    Service.checkAuthor(writer.href, function(err, result) {
        signals.signal("check_author_end", {
            author: writer,
            result: result
        });
        if (err) {
            console.log(timeStamp() + '|>booksUpdatesObserver. ERROR "' + writer.name + '" NOT checked. ' + err);
            isChecking = false; // unlock main loop to check other writers
            return;
        } else {
            if (result.success) {
                    console.log(timeStamp() + '|>booksUpdatesObserver. Author "' + writer.name + '" checked and he ' + ((result.authorHasNewUpdates) ? 'has NEW updates!' : 'has NO new updates'));
            }
            if (result.success && result.writer) {
                // set next update time
                writer = site.schedulers.author.calculateNextCheckTime(result.writer);
                console.log(timeStamp() + '|>booksUpdatesObserver. Author "' + writer.name + '" next time will be checked in ' + writer.checkUpdatesAt);
                // save writer
                site.storage.author.update(writer, function() {                    
                    isChecking = false; // unlock main loop to check other writers
                    Writer.publishUpdate(writer.id, writer);
                    return;
                });                
            } else {
                isChecking = false; // unlock main loop to check other writers
                return;                                
            }
        }
    });

}



exports.start = start;
exports.stop = stop;

start();