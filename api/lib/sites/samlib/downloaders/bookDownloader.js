var http = require('http');
var iconv = require('iconv-lite');
var zlib = require('zlib');

exports.download = function(url, completed) {
    var page = "";
    http.get(url, function(res) {
        if (200 == res.statusCode) {
            var chunks = [];
            res.on('data', function(chunk) {
                chunks.push(chunk);
            });
            res.on('end', function() {
                // var decodedBody = iconv.decode(Buffer.concat(chunks), 'win1251');
                // completed(null, decodedBody);
                var encoding = res.headers['content-encoding'];
                if (encoding && encoding === 'gzip') {
                    var buf = Buffer.concat(chunks);
                    zlib.unzip(buf, function(err, res) {
                        completed(null, iconv.decode(res, 'win1251'));
                    });
                } else {
                    var decodedBody = iconv.decode(Buffer.concat(chunks), 'win1251');
                    //console.log(decodedBody);
                    completed(null, decodedBody);
                }
            });
        } else {
            completed("Error getting page");
        };
    }).on('error', function(e) {
        completed("Got error: " + e.message);
    });
};