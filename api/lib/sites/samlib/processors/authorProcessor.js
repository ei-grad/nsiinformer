///////////////////////////////////////////
// join downloads and parse
///////////////////////////////////////////

function get(site, callback) {

    if (!site)
        return callback('[authorProcessor] ERROR Missed parameters.');
    site.downloaders.author.download(site.urlParts.href, function(error, data) {
        if (error) {
            console.log('[authorProcessor]|>ERROR: ' + error);
            callback('[authorProcessor]|>ERROR: ' + error);
        } else {
            site.parsers.author.parse(data, site.urlParts, function(error, result) {
                if (error) {
                    console.log('[authorProcessor]|>ERROR: ' + error);
                    callback('[authorProcessor]|>ERROR: ' + error);
                } else {
                    callback(null, result);
                }
            });
        }
    });
}


exports.get = get;