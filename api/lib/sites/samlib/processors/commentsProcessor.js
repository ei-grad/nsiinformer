///////////////////////////////////////////
// join downloads and parse
///////////////////////////////////////////

function get(site, callback) {

	if (!site)
		return callback('[commentsProcessor] ERROR Missed parameters.');

    site.downloaders.comments.download(site.urlParts, '', function(error, data) {
        if (error) {
            console.log('[commentsProcessor]|>ERROR: ' + error);
            callback('[commentsProcessor]|>ERROR: ' + error);
        } else {
            site.parsers.comments.parse(data, site.urlParts, function(error, result) {
                if (error) {
                    console.log('[commentsProcessor]|>ERROR: ' + error);
                    callback('[commentsProcessor]|>ERROR: ' + error);
                } else {
                	callback(null, result);
                }
            });
        }
    });
}


exports.get = get;