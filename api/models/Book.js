/**
 * Book.js
 *
 * @description :: This is a book (+ comments page) model
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

    connection: 'someMongodbServer',

    attributes: {
        siteTemplate: 'string',
        site: 'string',
        path: 'string',
        href: 'string',
        title: 'string',
        size: 'string',
        prevSize: 'string',
        annotation: 'string',
        annotationHtml: 'string',
        annotationHash: 'string',
        hash: 'string',
        rating: 'string',
        group: 'string',
        categories: 'array',
        lastCommentNumber: 'integer',
        lastCommentArchive: 'integer',
        commentsDescription: 'string',
        commentsPath: 'string',
        commentsHref: 'string',
        commentsPages: 'integer',
        commentsArhives: 'integer',
        commentsUpdateDetectedAt: 'datetime',
        commentsHasUpdates: 'boolean',
        checkCommentsAt: 'datetime',
        checkComments: 'boolean',
        checkUpdates: 'boolean',
        isUpdated: 'boolean',        
        hasUpdates: 'boolean',
        updateDetectedAt: 'datetime',
        writer: {
            model: 'Writer'
        },
        comments: {
            collection: 'Comment',
            via: 'book'
        },
        logs: {
            collection: 'UpdateLogItem',
            via: 'book'
        }
    }
};