module.exports = {
	 get: function(req, res) {	 	
		var query = {};
		if (req.param('writer')){
			query.writer = req.param('writer');
		}
		if (req.param('book')){
			query.book = req.param('book');
		}
		// query.order = {
		// 	updateDetectedAt: -1
		// }
		// console.log('Getting log items');
		// console.log(query);
		UpdateLogItem.find(query).sort({updateDetectedAt: -1}).exec(function(err, items) {
            if (err){
                 return res.json({error: err});
             }else{
             	// console.log("returning:");
             	// console.log(items);
                 return res.json(items);
             }            
        });
    }, 
};

