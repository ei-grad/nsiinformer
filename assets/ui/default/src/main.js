require('insert-css')(require('./app.css'));

require('./js/theme_init.js'); // lib for managing (initializing) jquery theme plugings
require('./lib/utils.js');
require('./lib/states.js');
require('./lib/db.js');
require('./lib/startup.js');

var Vue = require('vue');

new Vue({
    el: '#app',
    components: {
        appheader: require('./components/appheader'),
        leftsidebar: require('./components/leftsidebar'),
        maincontainer: require('./components/maincontainer')
    },
    // require html enabled by the partialify transform
    template: require('./app.html'),
    data: function() {
        return {
            toggle: $states.get('pageLayoutToggle') ? "" : "toggle"
        }
    },
    created: function() {
        this.$on('leftsidemenu_changes', function(item) {
            this.$broadcast('leftsidemenu_changes', item);
        });
        this.$on('change_view', function(params) {
            this.$broadcast('leftsidemenu_changes', params);
        });
        this.$on('dialog_data_set', function(item) {
            this.$broadcast('dialog_data_set', item);
        });
        this.$on('author_selected', function(item) {
            this.$broadcast('author_selected', item);
        });
        this.$on('author_selected_forcerefresh', function(item) {
            this.$broadcast('author_selected_forcerefresh', item);
        });
        this.$on('render_book_text', function(item) {
            this.$broadcast('render_book_text', item);
        });
    }
});