require('insert-css')(require('./style.css'));

module.exports = {
    template: require('./template.html'),
    replace: true,
    data: function() {
        return {
            expanded: false,
            visible: false            
        };
    },
    created: function() {
        this.getGroupExpansionStatus();
        this.getVisibilityStatus();
        var self = this;
        Signals.receive("groupExpansionChanged", function(group) {
            if (group == self.group) {
                self.getGroupExpansionStatus();
                self.getVisibilityStatus();
            }
        });
        Signals.receive("booksViewTypeChanged", function() {
            self.getVisibilityStatus();
        });
    },
    computed: {
        prevSizeVisible: function(){
            return this.prevSize && this.prevSize!==this.size;
        },
        updateDetectedAtFormated: function() {
            return moment(this.updateDetectedAt).format('DD MMMM YYYY, HH:mm');
        },
        shevrone: function() {
            return this.expanded ? 'fa-chevron-down' : 'fa-chevron-up';
        },
        books_count: function() {
            var groupIndex = _.findIndex($states.booksGroups, {
                group: this.group
            });
            if (groupIndex >= 0) {
                return $states.booksGroups[groupIndex].books_count;
            }
            return '';
        }
    },
    methods: {
        getVisibilityStatus: function() {
            var books_updated_count = 0;
            var groupIndex = _.findIndex($states.booksGroups, {
                group: this.group
            });
            if (groupIndex >= 0) {
                books_updated_count = $states.booksGroups[groupIndex].books_updated_count;
            }
            this.visible = this.expanded &&
                ($states.booksViewType == 'unread' ?
                (this.hasUpdates || !books_updated_count) : // if show unread only, but we dont have updated books in this group- show all of them
                true); // if show all - show all
        },
        getGroupExpansionStatus: function() {
            var groupIndex = _.findIndex($states.booksGroups, {
                group: this.group
            });
            if (groupIndex >= 0) {
                this.expanded = $states.booksGroups[groupIndex].expanded;
            }
        },
        onClick: function(event) {
            var self = this;
            window.socket.get("/book/getText", {
                id: this.id,
            }, function gotResponse(resData, jwres) {
                if (resData.error) {
                    $utils.showDialog('Ошибка получения текста: ' + resData.error, 'Менеджер закачек');
                    return;
                }
                // NOT WORKING!!!!
                // self.$dispatch('change_view', {
                //     view: 'bookreader',
                //     data: resData.text
                // }); // show bookreader
                self.$dispatch('leftsidemenu_changes', 'bookreader'); // show bookreader
                $states.currentBook = {
                    text: resData.text,
                    data: {
                        title: self.title,
                        authorName: $states.selectedAuthor.name
                    }
                };
                self.$dispatch('render_book_text', $states.currentBook); // send text to bookreader
                window.socket.get("/book/toggleHasUpdates", {
                    id: self.id,
                    hasUpdates: 'false'
                }, function gotResponse(resData, jwres) {
                    if (resData.error) {
                        $utils.showDialog('Ошибка переключения: ' + resData.error, 'Менеджер проверок', self);
                        self.hasUpdates = true; // set status back
                    }
                });
                self.hasUpdates = false;
            });
        },
        onToggleUpdates: function(event) {
            event.preventDefault();
            var self = this;
            window.socket.get("/book/toggleHasUpdates", {
                id: self.id,
                hasUpdates: 'false'
            }, function gotResponse(resData, jwres) {
                if (resData.error) {
                    $utils.showDialog('Ошибка переключения: ' + resData.error, 'Менеджер проверок', self);
                    self.hasUpdates = true; // set status back
                }
            });
            self.hasUpdates = false;

        },
        onGroupClicked: function(event) {
            event.preventDefault();
            var groupIndex = _.findIndex($states.booksGroups, {
                group: this.group
            });
            if (groupIndex >= 0) {
                $states.booksGroups[groupIndex].expanded = !$states.booksGroups[groupIndex].expanded;
                Signals.signal('groupExpansionChanged', this.group);
                Signals.signal('updateBooksLayout');
            }
        },
        onCommentsClick: function(){
             Signals.signal("showFastComments", this);
        }
    }
};