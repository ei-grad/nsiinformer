//require('insert-css')(require('./style.css'))

module.exports = {

    template: require('./template.html'),
    replace: true,
    data: function() {
        return {
            title: 'title',
            body: ''
        }
    },
    created: function() {
        var self = this;
        // this.$on('dialog_data_set', function(data) {
        //     self.title = data.title;
        //     self.body = data.body;
        // });
        $states.set('dialog_instance', this);
    },
    methods: {
        show: function(body, title) {
            this.body = body;
            this.title = title;
            $('#DarkNoShadowModal').modal('show');
        }
        // show: function() {
        //     $('#DarkNoShadowModal').modal('show');
        // }
    }
}