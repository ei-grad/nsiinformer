// require('insert-css')(require('./style.css'))

module.exports = {
    template: require('./template.html'),
    replace: true,
    data: function() {
        return {
            currentView: 'authors'
        }
    },
    created: function() {
        var Vue = require('vue');
        Vue.component("books", require('./../bookslist/index.js'));
        Vue.component("authors", require('./../authorspage/index.js'));
        Vue.component('dialog', require('./../dialog/index.js'));
        Vue.component('inputdialog', require('./../inputdialog/index.js'));
        Vue.component('bookreader', require('./../bookreader/index.js'));
        // Vue.component('fastcomments', require('./../fastcomments/index.js'));
        // NOT WORKIUNG!!!!!!!!!!!!
        // this.$on('change_view', function(params) {
        //     self.currentView = params.view;
        // });                

        context.init({
            fadespeed: 100,
            filter: function($obj) {},
            above: 'auto',
            preventDoubleContext: true,
            menuOnClick: true // show menu on left button click
        });

    },
    ready: function() {
        var self = this;
        this.$on('leftsidemenu_changes', function(item) {
            self.currentView = item;
        });
    }
}