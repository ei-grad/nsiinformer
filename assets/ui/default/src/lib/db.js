// db = {
// 	authors: 	new LocalCollection(),
// 	books:  	new LocalCollection(),
// 	comments:  	new LocalCollection(),
// 	log:	 	new LocalCollection()
// }

// window.socket.get("/writer", function(resData, jwres) {
// 	if (resData){
// 		_(resData).forEach(function(val){
// 			db.authors.insert(val);
// 		});
// 		console.log('>loaded ' + resData.length + ' authors');
// 	}
// });

var Author = Backbone.Model.extend({
	url: '/writer'
  });

var Book = Backbone.Model.extend({
	url: '/book'
  });

db = {
	//authors: 	new Backbone.Collection([], { model: Author, url: '/writer' }),
	// authors: 	Backbone.Collection.extend({ model: Author }), 
	// books:  	new LocalCollection(),
	// comments:  	new LocalCollection(),
	// log:	 	new LocalCollection()
}

db.authors = new Backbone.Collection([], { model: Author });
db.authors.url='/writer/getAll';

db.books = new Backbone.Collection([], { model: Book });
db.books.url='/book/getBy';
//Documents.fetch({data: {page: 3}})

//db.authors = Backbone.Collection.extend({ model: Author,  url: '/writer' })