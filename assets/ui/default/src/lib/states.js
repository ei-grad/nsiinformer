$states = {
    keys: {},
    set: function(key, value) {
        this.keys[key] = value;
    },
    get: function(key) {
        return this.keys[key];
    }
}